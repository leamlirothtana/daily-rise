import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Record from './src/screen/Record/Record';
import AddRecords from './src/screen/AddRecords/AddRecords';
import Activity from './src/screen/Activity/Activity';
import Dashboard from './src/screen/Dashboard/Dashboard';
import Started from './src/screen/Started/Started';
import Profile from './src/screen/Profile/Profile';
import Explore from './src/screen/Explore/Explore';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();
console.disableYellowBox = true;

function TabNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Activity} />
      <Tab.Screen name="Statistics" component={Dashboard}/>
      <Tab.Screen name="Trending" component={Explore}/>
      <Tab.Screen name="Profile" component={Profile}/>
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen options={{headerShown: false}} name="Started" component={Started} />
        <Stack.Screen options={{headerShown: false}} name="Record" component={Record} />
        <Stack.Screen options={{headerShown: false}} name="AddRecords" component={AddRecords} />
        <Stack.Screen options={{headerShown: false}} name="TabNavigation" component={TabNavigation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}