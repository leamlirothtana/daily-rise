## Please run following command below

To install all dependancies please run:
```
npm install
```

To install cocapod please run: 
```
cd ios && pod install
```

To start IOS simulator please run: 
```
npm run ios
```

To start services with clearing cached please run:
```
npm run dev
```