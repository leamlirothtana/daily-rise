import React from 'react';
import {ScrollView, View, Text, Image, Dimensions} from 'react-native';
import styles from './Profile.scss';

var width = Dimensions.get('window').width; //full width

const Profile = () => {
    return (
        <ScrollView style={styles.container}>
            <View style={[styles.profile_header]}>
                <Text style={styles.screen_title}>Profile</Text>
                <View style={styles.profile_section}>
                    <Image source={require('./user-icon.png')} style={styles.profile_picture}/>
                    <Text style={styles.profile_name}>Lirothtana Leam</Text>
                    <Text style={styles.profile_point}>200 points | Professional</Text>
                </View>
            </View>
            <View>
                <Text style={styles.most_recent_text}>Most Recent</Text>
                <Text style={styles.pushup_text}>PUSHUP</Text>
                <Image source={require('./graphic.png')} style={[styles.profile_graphic, {width: width - 30}]}/>
                <View style={styles.profile_content_section}>
                    <Text style={styles.section_title}>Badges</Text>
                    <Image source={require('./Badges.png')} style={[styles.profile_content_image, {width: width - 30}]}/>
                </View>
                <View style={styles.profile_content_section}>
                    <Text style={styles.section_title}>Badges</Text>
                    <Image source={require('./Badges.png')} style={[styles.profile_content_image, {width: width - 30}]}/>
                </View>
            </View>
        </ScrollView>
    )
}

export default Profile;