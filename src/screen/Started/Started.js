import React, {useState} from 'react';
import {ScrollView, View, Text, Image, ImageBackground, StyleSheet, Button, TouchableHighlight} from 'react-native';
import SignUpModal from './../../components/SignUpModal/SignUpModal'
import Attention from './../../components/Attention/Attention'

const Started = ({navigation}) => {
    const [modal, setModal] = useState(false);
    const [attention, setAttention] = useState(false);
    const handleOpenModal = () => {
        setModal(true);
    }
    const handleOpenNextModal = (bool) => {
        setModal(false);
        if(!bool) {
            setAttention(true);
        } else {
            navigation.navigate('TabNavigation')
        }
    }
    const handleOpenHome = () => {
        navigation.navigate('TabNavigation')
        setAttention(false);
    }
    return (
    <View style={styles.container}>
        <ImageBackground source={require('./person-rope-woman-model-fashion-musician-exercise-black-healthy-workout-performance-art-stage-performance-singing-fit-961074.png')}   style={styles.image}>
            <Text style={styles.text}>DailyRise</Text>
            <View> 
                <TouchableHighlight style={styles.TouchableHighlight} onPress={handleOpenModal}>
                    <Text style = {styles.text1}>Get Started</Text>
                </TouchableHighlight>
            </View>  
        </ImageBackground>
        <SignUpModal visible={modal} onPress={handleOpenNextModal} onClose={(e) => setModal(e)}/>
        <Attention visible={attention} onPress={handleOpenHome} onClose={(e) => setModal(e)}/>
    </View>
    )
}


const styles = StyleSheet.create({
    container: {
    flex: 1,
    flexDirection: "column"
    },
    image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
    },
    text: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
    alignSelf: "center",
    paddingBottom: 550,
    //   fontFamily:"AKbalthom SuperheroKH"
    },
    text1: {
        
    alignSelf: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: 16

    },
    TouchableHighlight: {
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'#44CAAC',
    borderRadius:10,
    
    }

    

});

export default Started;