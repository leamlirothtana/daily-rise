import React from 'react';
import Slideshow from 'react-native-image-slider-show';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';

const AddRecords = ({navigation}) => {
  const handleOpenScreen = () => {
    navigation.navigate('Record')
  }

  return(
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>Statistics</Text>
        <Text style={styles.text1}>Add Records</Text>
      </View>
      <View style={styles.outdoor}>
        <Text style={styles.text2}>Outdoor</Text>
        <Slideshow 
          onPress={handleOpenScreen} 
          dataSource={[
            { url:'http://placeimg.com/640/480/any' },
            { url:'http://placeimg.com/640/480/any' },
            { url:'http://placeimg.com/640/480/any' }
          ]}
        />
      </View>
      <View style={styles.outdoor}>
        <Text style={styles.text2}> Indoor</Text>
        <Slideshow 
          onPress={handleOpenScreen} 
          dataSource={[
            { url:'http://placeimg.com/640/480/any' },
            { url:'http://placeimg.com/640/480/any' },
            { url:'http://placeimg.com/640/480/any' }
          ]}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column"
      },
    header:{
      paddingTop: 70,
      paddingHorizontal: 30
    },
    text:{
      color:"grey",
      fontSize: 18
    },
    text1: {
      fontSize: 30
    },
    outdoor:{
      paddingVertical: 20,
      paddingHorizontal: 30
    },
  
    text2:{
      fontSize: 20,
      // fontWeight: "bold"
    }
  });

export default AddRecords;