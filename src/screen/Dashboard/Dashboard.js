import React from 'react';
import {ScrollView, View, Dimensions, Text, TouchableHighlight} from 'react-native';
import DashboardCard from './../../components/DashboardCard/DashboardCard';
import styles from './Dashboard.scss';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const Dashboard = ({navigation}) => {
    return (
        <ScrollView style={[styles.container]}> 
            <View style={[styles.dashboard_header]}>
                <View>
                    <Text style={styles.pre_title}>Statistics</Text>
                    <Text style={styles.screen_title}>Dashboard</Text>
                </View>
                <TouchableHighlight style={styles.dashboard_plus_button} onPress={() => navigation.navigate('AddRecords')}>
                    <Text style={styles.dashboard_plus_button_text}>+</Text>
                </TouchableHighlight>
            </View>
            <View>
                <DashboardCard score="100" type="pushup"/>
                <DashboardCard score="80" type="running" typeOfScore="KM"/>
                <DashboardCard score="90" type="yoga" typeOfScore="Minutes"/>
            </View>
        </ScrollView>
    )
}

export default Dashboard;