import React from 'react';
import {ScrollView, View, Text} from 'react-native';
import VideoComponent from '../../components/VideoComponent/VideoComponent';
import styles from './Explore.scss';

const Explore = () => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.explore_header}>
                <Text style={styles.screen_title}>Explore</Text>
                <Text style={styles.pre_title}>Enter excercise or group</Text>
            </View>
            <View>
                <Text style={styles.explore_suggestion_text}>Suggestion</Text>
                <VideoComponent />
                <VideoComponent />
                <VideoComponent />
            </View>
        </ScrollView>
    )
}

export default Explore;