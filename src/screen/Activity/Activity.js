import React from 'react';
import {ScrollView, View, Text, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Card from './../../components/Card/Card';
import styles from './Activity.scss';

const Activity = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.screen_title}>Activity</Text>
            <View style={[styles.user_profile_section]}>
                <View style={[styles.user_info]}>
                    <Image source={require('./user-icon.png')} style={[styles.user_profile]}/>
                    <View>
                        <Text style={[styles.runing_title]}>Pungponhavoan TEP</Text>
                        <Text style={[styles.time_ago]}>2 hours ago</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.items_list}>
                        <LinearGradient start={{x: 0.3, y: 0}} end={{x: 1, y: 0}} colors={['#FFF', 'rgba(0,0,0,.2)']} style={styles.item}>
                            <Text>0 KM</Text>
                            <Text>JOGGING</Text>
                        </LinearGradient>
                        <LinearGradient start={{x: .3, y: 0}} end={{x: 1, y: 0}} colors={['#FFF', 'rgba(0,0,0,.2)']} style={styles.item}>
                            <Text>0 MIN</Text>
                            <Text>YOGA</Text>
                        </LinearGradient>
                    </View>
                    <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent egestas ex id felis eleifend, nec interdum nibh scelerisque.</Text>
                </View>
            </View>
            <Card />
            <Card 
                username="Lirothtana Leam"
                status="Kratie running"
                timeago= '2 hours ago'
                kilometer="5 KM"
            />
        </ScrollView>
    )
}

export default Activity;