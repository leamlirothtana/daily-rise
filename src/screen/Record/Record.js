import React, {useState} from 'react';
import {View, Text, TouchableHighlight, Picker} from 'react-native';
import styles from './Record.scss';

const Record = ({navigation}) => {
    const [selectedValue, setSelectedValue] = useState("java");
    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.pre_title}>Statistics</Text>
                <Text style={styles.screen_title}>Add Records</Text>
                <Text style={styles.question_text}>How long have run today ?</Text>
            </View>
            <View style={styles.picker_section}>
                <Picker
                    selectedValue={selectedValue}
                    style={{ height: 50, width: 150 }}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                >
                    {
                        Array.from(new Array(100), (item, i) => <Picker.Item label={`${(i + 1).toString()} KM`} value={i + 1} key={i}/>)
                    }
                </Picker>
            </View>
            
            <View style={styles.record_footer}>
                <TouchableHighlight onPress={() => navigation.navigate('Statistics')} style={styles.record_continue_button}>
                    <Text style={styles.record_continue_button_text}>Continue</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}

export default Record;