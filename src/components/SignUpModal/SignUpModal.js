import React, {useState} from 'react';
import {View, Text, Dimensions, TouchableHighlight, Image} from 'react-native';
import Modal, { ModalContent, ModalTitle } from 'react-native-modals';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles from './SignUpModal.scss';

const SignUpModal = ({visible = true, onPress, onClose}) => {
    // const [display, setDisplay] = useState(visible)
    const handlePress = (type) => {
        if(type == 'guest') {
            onPress(false);
        } else {
            onPress(true);
        }
    }
    const handleClose = () => {
        onClose(false);
    }
    return (<View style={styles.container}>
        <Modal
            visible={visible}
            onTouchOutside={handleClose}
            style={styles.modal}
        >
            <ModalContent style={[styles.modal_content, { width: width, height: height/2}]}>
                <Text style={[styles.modal_title, styles.text_center]}>Log in or Sign up</Text>
                <Text style={[styles.text_center, styles.modal_description]}>Welcome to Daily Rise, First thing first log in or sign up so that we can begin</Text>
                <TouchableHighlight style={[styles.modal_button]} onPress={() => handlePress('')}>
                    <Text style={[styles.text_center]}>
                        Continue with Phone Number
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.modal_button]} onPress={() => handlePress('')}>
                    <Text style={[styles.text_center]}>
                        <Image source={require('./facebook.png')} style={styles.modal_icon}/>  Continue with Facebook
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.modal_button]} onPress={() => handlePress('')}>
                    <Text style={[styles.text_center]}>
                        <Image source={require('./google.png')} style={styles.modal_icon}/>  Continue with Google
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.modal_button]} onPress={() => handlePress('guest')}>
                    <Text style={[styles.text_center]}>Start as guest</Text>
                </TouchableHighlight>
            </ModalContent>
        </Modal>
    </View>)
}

export default SignUpModal;