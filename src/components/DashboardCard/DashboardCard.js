import React from 'react';
import {View, Text, Image, Dimensions, TouchableHighlight} from 'react-native';
import styles from './DashboardCard.scss';

var width = Dimensions.get('window').width; //full width

const DashboardCard = ({
    type,
    score,
    typeOfScore=""
}) => {
    return (
        <View style={styles.dashboard_card}>
            <View style={styles.dashboard_card_header}>
                <Text style={[styles.dashboard_card_title]}>{type.toUpperCase()}</Text>
                <View style={[styles.dashboard_card_header_tabs]}>
                    <Text style={[styles.dashboard_card_header_tab]}>5-April</Text>
                    <Text style={[styles.dashboard_card_header_tab]}>6-April</Text>
                    <Text style={[styles.dashboard_card_header_tab]}>7-April</Text>
                    <Text style={[styles.dashboard_card_header_tab]}>Today</Text>
                </View>
            </View>
            <View>
                <Text style={styles.score_pre_text}>Score</Text>
                <Text style={styles.score_text}>{score} <Text style={styles.typeOfScore}>{typeOfScore.toUpperCase()}</Text></Text>
                <Image style={[styles.dashboard_card_image, {width: width - 70}]} source={require('./graphic.png')} />
            </View>
            <View style={[styles.dashboard_card_footer]}>
                <TouchableHighlight style={styles.dashboard_card_button} onPress={() => alert("press")}>
                    <Text style={styles.dashboard_card_button_text}>→</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}

export default DashboardCard;