import React from 'react';
import {View, Text, ImageBackground, Dimensions, Image} from 'react-native';
import Video from 'react-native-video';
import styles from './VideoComponent.scss';

var width = Dimensions.get('window').width; //full width
const VideoComponent = ({
    image= require('./yoga.png')
}) => {
    return (
        <View style={styles.container}>
            {/* <Video source={{uri: "https://www.youtube.com/embed/sTANio_2E0Q"}}   // Can be a URL or a local file.
                ref={(ref) => {
                    this.player = ref
                }}                                      // Store reference
                onBuffer={this.onBuffer}                // Callback when remote video is buffering
                onError={this.videoError}               // Callback when video cannot be loaded
                style={styles.backgroundVideo} /> */}
            <ImageBackground source={image} style={[styles.background, {width: width - 30}]} imageStyle={{ borderRadius: 10 }}>
                <Text style={[styles.video_title]}>YOGA</Text>
                <Text style={[styles.video_sub_title]}>Indoor Activity</Text>
                <View style={[styles.video_sction, {width: width - 30}]}>
                    <Image source={require('./playIcon.png')} style={[styles.playicon]} onPress={() => alert("this feature will comming soon!")}/>
                </View>
            </ImageBackground>
        </View>
    )
}

export default VideoComponent;