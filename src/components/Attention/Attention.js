import React, {useState} from 'react';
import {View, Text, Dimensions, TouchableHighlight} from 'react-native';
import Modal, { ModalContent } from 'react-native-modals';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles from './Attention.scss';

const Attention = ({visible = true, onPress, onClose}) => {
    // const [display, setDisplay] = useState(visible);
    const handlePress = () => {
        onPress(true);
    }
    const handleClose = () => {
        onClose(false)
    }
    return (<View style={styles.container}>
        <Modal
            visible={visible}
            onTouchOutside={handleClose}
            style={styles.modal}
        >
            <ModalContent style={[styles.modal_content, { width: width, height: height/2}]}>
                <View>
                    <Text style={[styles.modal_title, styles.text_center]}>Attention</Text>
                    <Text style={[styles.text_center, styles.modal_description]}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent egestas ex id felis eleifend, nec interdum nibh scelerisque.
                    </Text>
                </View>
                <TouchableHighlight style={[styles.modal_agree_button]} onPress={handlePress}>
                    <Text style={[styles.text_center, styles.agree_text]}>Agree</Text>
                </TouchableHighlight>
            </ModalContent>
        </Modal>
    </View>)
}

export default Attention;