import React from 'react';
import styles from './Card.scss';
import {ImageBackground, View, Image, Text, Dimensions} from 'react-native'

var width = Dimensions.get('window').width; //full width

const Card = ({
    username="Lydeth PIDOR",
    status="London running",
    timeago= '3 hours ago',
    kilometer="10 KM",
    image=require('./background-running.jpg')
}) => {
    return (
        <View style={[styles.card]}>
            <View style={[styles.user_info]}>
                <Image source={require('./user-icon.png')} style={[styles.user_profile]}/>
                <View>
                    <Text style={[styles.runing_title]}>{username} - {status}</Text>
                    <Text style={[styles.time_ago]}>{timeago}</Text>
                </View>
            </View>
            <ImageBackground source={image} style={[styles.background, {width: width - 30}]}>
                <Text style={[styles.pre_text]}>Completed a</Text>
                <Text style={[styles.card_kilometer]}>{kilometer}</Text>
                <Text style={[styles.pre_text]}>of running today</Text>
            </ImageBackground>
        </View>
    )
}

export default Card;